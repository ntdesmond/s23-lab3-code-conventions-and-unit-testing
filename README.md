# Lab3 -- Unit testing

## Homework task

As a homework you will need to test each of the functions in `src/main/java/com/hw/db/controllers/threadController.java` using both mocks and stubs. After that you should add your tests to the pipeline. Tests that you write should pass linter checks.
**Lab is counted as done, if pipelines are passing and tests are developed**

## Tests for ThreadController

see [ThreadControllerTests.java](./src/test/java/com.hw.db/controllers/ThreadControllerTests.java)

Tests are included in the CI.

## Pipeline status

[![pipeline status](https://gitlab.com/ntdesmond/s23-lab3-code-conventions-and-unit-testing/badges/master/pipeline.svg)](https://gitlab.com/ntdesmond/s23-lab3-code-conventions-and-unit-testing/-/commits/master) 