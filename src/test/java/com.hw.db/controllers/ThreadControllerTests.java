package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

class ThreadControllerTests {
    private String thread_id;
    private String thread_slug;
    private Thread thread;
    private User user;
    private Post post;

    @BeforeEach
    void setVariables() {
        thread_id = "1337";
        thread_slug = "slug";
        user = new User("author","hate@this.hw", "name", "sus");
        thread = new Thread(
            "author",
            Timestamp.from(Instant.MIN),
            "forum",
            "message",
            thread_slug,
            "title",
            0
        );
        thread.setId(1337);
        post = new Post();
        post.setAuthor("author");
    }

    @Nested
    @DisplayName("Thread search")
    class ThreadSearch {
        @Test
        @DisplayName("can be done by slug")
        void threadIsFoundBySlug() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadBySlug("slug"))
                    .thenReturn(thread);
                ThreadController controller = new ThreadController();
                assertEquals(thread, controller.CheckIdOrSlug(thread_slug));
            }
        }

        @Test
        @DisplayName("can be done by ID")
        void threadIsFoundById() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1337))
                    .thenReturn(thread);
                ThreadController controller = new ThreadController();
                assertEquals(thread, controller.CheckIdOrSlug(thread_id));
            }
        }

        @Test
        @DisplayName("may fail throwing DataAccessException")
        void threadSearchFail() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                        .when(() -> ThreadDAO.getThreadById(1000))
                        .thenThrow(new EmptyResultDataAccessException(1));
                ThreadController controller = new ThreadController();
                assertThrows(DataAccessException.class, () -> controller.CheckIdOrSlug("1000"));
            }
        }
    }

    @Nested
    @DisplayName("Creating posts")
    class PostCreation {
        private List<Post> posts;

        @BeforeEach
        void createPostsList() {
            posts = new ArrayList<>();
            posts.add(post);
        }

        @Test
        @DisplayName("sets an author, thread, and forum")
        void postsAreCreated() {
            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class);
                MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)
            ) {
                AtomicReference<List<User>> users = new AtomicReference<>();
                userDAO
                    .when(() -> UserDAO.Info("author"))
                    .thenReturn(user);
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1337))
                    .thenReturn(thread);
                threadDAO
                    .when(() -> ThreadDAO.createPosts(any(), any(), any()))
                    .then(
                        answer -> {
                            users.set(answer.getArgument(2));
                            return null;
                        }
                    );

                ThreadController controller = new ThreadController();

                // Since the code creator fails to use generics properly
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.createPost(thread_id, posts);

                // The returned response
                assertEquals(HttpStatus.CREATED, response.getStatusCode());
                assertEquals(posts, response.getBody());

                // User passed to ThreadDAO.createPosts(thread, posts, users)
                assertEquals(1, users.get().size());
                assertEquals(user, users.get().get(0));

                // Post details
                assertEquals("author", posts.get(0).getAuthor());
                assertEquals((Integer)1337, posts.get(0).getThread());
                assertEquals("forum", posts.get(0).getForum());
            }
        }

        @Test
        @DisplayName("fails on missing parent posts in thread")
        void missingParentPosts() {
            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class);
                MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)
            ) {
                AtomicReference<List<User>> users = new AtomicReference<>();
                userDAO
                    .when(() -> UserDAO.Info("author"))
                    .thenReturn(user);
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1337))
                    .thenReturn(thread);

                // Throw "posts are in another thread" error (the code sucks btw)
                threadDAO
                    .when(() -> ThreadDAO.createPosts(any(), any(), any()))
                    .thenThrow(new DuplicateKeyException("Parent is in another castle."));

                ThreadController controller = new ThreadController();

                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.createPost(thread_id, posts);

                // The returned response
                assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
            }
        }

        @Test
        @DisplayName("fails on missing thread")
        void missingThread() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1000))
                    .thenThrow(new EmptyResultDataAccessException(1));
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.createPost("1000", posts);
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Nested
    @DisplayName("Listing posts")
    class PostListing {
        @Test
        @DisplayName("works properly")
        void postsListed() {
            List<Post> posts = new ArrayList<>();
            posts.add(new Post());

            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                        .when(() -> ThreadDAO.getThreadById(1337))
                        .thenReturn(thread);
                threadDAO
                        .when(() -> ThreadDAO.getPosts(1337, 1, 0, "", false))
                        .thenReturn(posts);
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.Posts(thread_id, 1, 0, "", false);
                assertEquals(HttpStatus.OK, response.getStatusCode());
                assertEquals(posts, response.getBody());
            }
        }

        @Test
        @DisplayName("fails on missing thread")
        void missingThread() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1000))
                    .thenThrow(new EmptyResultDataAccessException(1));
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.Posts("1000", 1, 0, "", false);
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Nested
    @DisplayName("Editing thread")
    class ThreadEditing {
        @Test
        @DisplayName("works properly")
        void threadEdited() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1337))
                    .thenReturn(thread);
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.change(thread_id, thread);
                assertEquals(HttpStatus.OK, response.getStatusCode());
                assertEquals(thread, response.getBody());
            }
        }

        @Test
        @DisplayName("fails on missing thread")
        void missingThread() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1000))
                    .thenThrow(new EmptyResultDataAccessException(1));
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.change("1000", thread);
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Nested
    @DisplayName("Getting thread info")
    class ThreadInfo {
        @Test
        @DisplayName("works properly")
        void threadEdited() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1337))
                    .thenReturn(thread);
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.info(thread_id);
                assertEquals(HttpStatus.OK, response.getStatusCode());
                assertEquals(thread, response.getBody());
            }
        }

        @Test
        @DisplayName("fails on missing thread")
        void missingThread() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1000))
                    .thenThrow(new EmptyResultDataAccessException(1));
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.info("1000");
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }

    @Nested
    @DisplayName("Creating votes")
    class VoteCreation {
        private Vote vote;

        @BeforeEach
        void createVote() {
            vote = new Vote("author", 10);
        }

        @Test
        @DisplayName("works properly")
        void voteCreated() {
            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class);
                MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)
            ) {
                userDAO
                    .when(() -> UserDAO.Info("author"))
                    .thenReturn(user);
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1337))
                    .thenReturn(thread);
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.createVote(thread_id, vote);
                assertEquals(HttpStatus.OK, response.getStatusCode());
                assertEquals(thread, response.getBody());
            }
        }

        @Test
        @DisplayName("fails on ??? i don't get the reason")
        void duplicateKeyOnVotes() {
            try (
                MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class);
                MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)
            ) {
                userDAO
                    .when(() -> UserDAO.Info("author"))
                    .thenReturn(user);
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1337))
                    .thenReturn(thread);
                threadDAO
                    .when(() -> ThreadDAO.createVote(thread, vote))
                    .thenThrow(new DuplicateKeyException("Vote exists already"));

                // The DuplicateKeyException in createVote() says about missing parent posts,
                // but the method doesn't touch the parent posts... Anyway.
                threadDAO
                    .when(() -> ThreadDAO.change(vote, thread.getVotes()))
                    .thenThrow(new DuplicateKeyException("But what does that mean?"));

                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.createVote(thread_id, vote);
                assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
            }
        }

        @Test
        @DisplayName("fails on missing thread")
        void missingThread() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO
                    .when(() -> ThreadDAO.getThreadById(1000))
                    .thenThrow(new EmptyResultDataAccessException(1));
                ThreadController controller = new ThreadController();
                @SuppressWarnings("rawtypes")
                ResponseEntity response = controller.createVote("1000", vote);
                assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
            }
        }
    }
}
